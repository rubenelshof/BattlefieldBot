var Discord = require("discord.js");
var bot = new Discord.Client();

//required modules
const mysql = require("mysql");
const request = require("request");
const getJSON = require("get-json");

//debug
const util = require("util");
console.log(util.inspect(Discord, false, null));

//discordtoken
var token = require('./discordtoken.js');
bot.login(token);

//db
var connection = require("./connection.js");

//console log
bot.on('ready', () => {
  console.log(`Logged in as: ${bot.user.username}`);
  bot.user.setStatus("online");
  bot.user.setActivity("Battlefield Stats");
});

//dbservercreate
bot.on("guildCreate", guild => {
  console.log("Trying to insert server " + guild.name + " into database");
  var info = {
    "servername": "'" + guild.name + "'",
    "serverid": guild.id,
    "ownerid": guild.ownerID,
    "prefix": "&"
  };

  connection.query("INSERT INTO servers SET ?", info, function (err) {
    if (err) {
      console.error(err);
    } else {
      console.log("Server added successfully");
    }
  });
});

//dbserverdelete
bot.on("guildDelete", guild => {
  console.log("Trying to remove server " + guild.name + " from database");
  connection.query("DELETE FROM servers WHERE serverid = '" + guild.id + "'", function (err) {
    if (err) {
      console.error(err);
    } else {
      console.log("Server removed successfully");
    }
  });
});

//help
bot.on('message', message => {
  if (!message.channel.dmchannel) {
    if (message.content === "!bfbothelp") {
      message.channel.send(
        "Commandlist \r" +
        "!bfbc2rank [Playername] = Players rank in Battlefield Bad Company 2 \r" +
        "!bfbc2skill [Playername] = Players skill in Battlefield Bad Company 2 \r" +
        "!bfbc2kd [Playername] = Players kill/death ratio in Battlefield Bad Company 2 \r" +
        "!bf3rank [Playername] = Players rank in Battlefield 3 \r" +
        "!bf3skill [Playername] = Players skill in Battlefield 3 \r" +
        "!bf3kd [Playername] = Players kill/death ratio in Battlefield 3 \r" +
        "!bf4rank [Playername] = Players rank in Battlefield 4 \r" +
        "!bf4skill [Playername] = Players skill in Battlefield 4 \r" +
        "!bf4kd [Playername] = Players kill/death ratio in Battlefield 4 \r" +
        "!bf1rank [Playername] = Players rank in Battlefield 1 \r" +
        "!bf1skill [Playername] = Players skill in Battlefield 1 \r" +
        "!bf1kd [Playername] = Players kill/death ratio in Battlefield 1"
      );
    }
  }
});

/*
//setplatform
bot.on('message', message => {
    if (message.content === "!bfbotsetpc") {
      var platformpc = "INSERT INTO platform (serverid, platform) VALUES (" + message.guild.id + ", 'pc') ON DUPLICATE KEY UPDATE platform = 'pc'";
      connection.query(platformpc, function (err) {
        if (err) throw err;
        console.log("Platform PC");
      });
    };
    if (message.content === "!bfbotsetxbox") {
      var platformxbox = "INSERT INTO platform (serverid, platform) VALUES (" + message.guild.id + ", 'xbox') ON DUPLICATE KEY UPDATE platform = 'xbox'";
      connection.query(platformxbox, function (err) {
        if (err) throw err;
        console.log("Platform Xbox");
      });
    };
    if (message.content === "!bfbotsetps") {
      var platformps = "INSERT INTO platform (serverid, platform) VALUES (" + message.guild.id + ", 'ps') ON DUPLICATE KEY UPDATE platform = 'ps'";
      connection.query(platformps, function (err) {
        if (err) throw err;
        console.log("Platform Playstation");
      });
    };
});
*/
//bfbc2stats
bot.on('message', message => {
  if (!message.channel.dmchannel) {
    //get content
    var content = message.content;
    var name = content.split(" ");
    var playername = name[1];
/*
    var platformselect = connection.query("SELECT platform FROM platform WHERE serverid = " + message.guild.id + "");

    if (platformselect === "xbox") {
        platformbfbcs = "360";
    } else if (platformselect === "ps") {
        platformbfbcs = "ps3";
    } else {
        platformbfbcs = "pc";
    }
    //api
    var bfbc2statsapi = "http://api.bfbcs.com/api/" + platformbfbcs + "?players=" + playername + "&fields=general";*/

    //api
    var bfbc2statsapi = "http://api.bfbcs.com/api/pc?players=" + playername + "&fields=general";

    //url
    var bfbc2stats = "http://bfbcs.com/";
    var url = bfbc2stats + playername;

    if (message.content === ("!bfbc2rank " + playername)) {
      getJSON(bfbc2statsapi, function (err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.found === 0) {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your rank is **" + res["players"][0]["rank"] + "**");
          };
        }
      });
    }
    if (message.content === ("!bfbc2skill " + playername)) {
      getJSON(bfbc2statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.found === 0) {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your skill score is **" + res["players"][0]["elo"].toFixed(0) + "**");
          };
        }
      });
    }
    if (message.content === ("!bfbc2kd " + playername)) {
      getJSON(bfbc2statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.found === 0) {
            message.channel.send("Player not identified");
          } else {
            var killsbfbc2 = (res["players"][0]["kills"]);
            var deathsbfbc2 = (res["players"][0]["deaths"]);
            message.channel.send("Your K/D is **" + (killsbfbc2 / deathsbfbc2).toFixed(2) + "**");
          };
        }
      });
    }
  }
});

//bf3stats
bot.on('message', message => {
  if (!message.channel.isPrivate) {
    //get content
    var content = message.content;
    var name = content.split(" ");
    var playername = name[1];

    //api
    var bf3statsapi = "http://api.bf3stats.com/pc/player/player=" + playername + "&opt=clear,rank,global";

    //url
    var bf3stats = "http://bf3stats.com/pc/";
    var url = bf3stats + playername;

    if (message.content === ("!bf3rank " + playername)) {
      getJSON(bf3statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.status == "notfound") {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your rank is **" + res.stats.rank.nr + "**");
          };
        }
      });
    }
    if (message.content === ("!bf3skill " + playername)) {
      getJSON(bf3statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.status == "notfound") {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your skill score is **" + res.stats.global.elo.toFixed(0) + "**");
          };
        }
      });
    }
    if (message.content === ("!bf3kd " + playername)) {
      getJSON(bf3statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.status == "notfound") {
            message.channel.send("Player not identified");
          } else {
            var killsbf3 = (res.stats.global.kills);
            var deathsbf3 = (res.stats.global.deaths);
            message.channel.send("Your K/D is **" + (killsbf3 / deathsbf3).toFixed(2) + "**");
          };
        }
      });
    }
  }
});

//bf4stats
bot.on('message', message => {
  if (!message.channel.isPrivate) {
    //get content
    var content = message.content;
    var name = content.split(" ");
    var playername = name[1];

    //api
    var bf4statsapi = "http://api.bf4stats.com/api/playerInfo?plat=pc&name=" + playername + "&opt=stats,extra";

    //url
    var bf4stats = "http://bf4stats.com/pc/";
    var url = bf4stats + playername;

    if (message.content === ("!bf4rank " + playername)) {
      getJSON(bf4statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.error == "notFound") {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your rank is **" + res.stats.rank + "**");
          };
        }
      });
    }
    if (message.content === ("!bf4skill " + playername)) {
      getJSON(bf4statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.error == "notfound") {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your skill score is **" + res.stats.skill + "**");
          };
        }
      });
    }
    if (message.content === ("!bf4kd " + playername)) {
      getJSON(bf4statsapi, function(err, res) {
        if (err) {
          console.error(err);
        } else {
          if (res.error == "notFound") {
            message.channel.send("Player not identified");
          } else {
            message.channel.send("Your K/D is **" + res.stats.extra.kdr.toFixed(2) + "**");
          };
        }
      });
    }
  }
});

//bf1stats
bot.on('message', message => {
  if (!message.channel.isPrivate) {
    //get content
    var content = message.content;
    var name = content.split(" ");
    var playername = name[1];

    //api
    var bftoken = require('./bftoken.js');
    var bf1basicstats = {url: "https://battlefieldtracker.com/bf1/api/Stats/BasicStats?platform=3&displayName=" + playername + "&game=tunguska",headers: {'trn-api-key': bftoken}};
    var bf1detailedstats = {url: "https://battlefieldtracker.com/bf1/api/Stats/DetailedStats?platform=3&displayName=" + playername + "&game=tunguska",headers: {'trn-api-key': bftoken}};

    //url
    var bf1stats = "https://battlefieldtracker.com/bf1/profile/pc/";
    var url = bf1stats + playername;

    //powered by
    var bftracker = "Powered by Tracker Network"

    if (message.content === ("!bf1rank " + playername)) {
      request(bf1basicstats, function (err, res, body) {
        if (err) {
          console.error(err);
        } else {
          if (!err && res.statusCode == 200) {
            var info = JSON.parse(body);
            message.channel.send("Your rank is **" + info.result.rank.number + "** \r" + "More stats at " + url + "\r" + bftracker);
          } else {
            message.channel.send("Player not identified");
          }
        }
      });
    };

    if (message.content === ("!bf1skill " + playername)) {
      request(bf1basicstats, function (err, res, body) {
        if (err) {
          console.error(err);
        } else {
          if (!err && res.statusCode == 200) {
            var info = JSON.parse(body);
            message.channel.send("Your skill score is **" + info.result.skill + "** \r" + "More stats at " + url + "\r" + bftracker);
          } else {
            message.channel.send("Player not identified");
          }
        }
      });
    };

    if (message.content === ("!bf1kd " + playername)) {
      request(bf1detailedstats, function (err, res, body) {
        if (err) {
          console.error(err);
        } else {
          if (!err && res.statusCode == 200) {
            var info = JSON.parse(body);
            message.channel.send("Your K/D is **" + info.result.kdr + "** \r" + "More stats at " + url + "\r" + bftracker);
          } else {
            message.channel.send("Player not identified");
          }
        }
      });
    };
  }
});